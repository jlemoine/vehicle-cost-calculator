import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate from 'vee-validate'
import BootstrapVue from 'bootstrap-vue'
import VueI18Next from '@panter/vue-i18next'
import App from './App.vue'
import { store } from './store'
import I18n from './i18n'
import routes from './routes'

// Css
import './fortawesome'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'chartjs-plugin-colorschemes'

import './app.scss'

// Configure Vue
Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(VueI18Next);
Vue.use(BootstrapVue);
Vue.use(VeeValidate, {fieldsBagName: 'formFields'});

const router = new VueRouter({
  routes // short for `routes: routes`
});

let i18n = I18n();

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app')
