export const SCALE = 12; 

/**
 * Calculate initial price
 *
 * @param vehicle
 * @returns {number}
 */
export const initial = (vehicle) => {
  let price = parseInt(vehicle.priceBuy);

  return price
};

/**
 * Get usage price in a step period
 * 
 * @param vehicle
 * @param steps
 * @returns {number}
 */
export const price = (vehicle, steps) => {
  let kmPerStep = parseInt(vehicle.kmTraveled / SCALE);
  let price = 0;

  // Add fuels
  for (let i in vehicle.fuels) {
    if (vehicle.fuels.hasOwnProperty(i)) {
      price += pricePerKilometerOfFuel(vehicle.fuels[i])
        * priceGrowth(parseFloat(vehicle.fuels[i].costGrowth), steps)
        * kmPerStep // Number of Km drove
        * SCALE // Number of Steps
    }
  }

  // Add invoices
  for (let i in vehicle.invoices) {
    if (vehicle.invoices.hasOwnProperty(i)) {
      price += triggerInvoiceAtInstant(vehicle.invoices[i], kmPerStep, steps)
    }
  }

  return price
};

/**
 * Get the total price of 1km
 * 
 * @param vehicle
 * @param steps
 * @param sale
 * @returns {number}
 */
export const kmPrice = (vehicle, steps, sale = false) => {
  return kmFuelPrice(vehicle, steps)
      + kmCarPrice(vehicle, steps, sale ? vehicle.priceSell : 0)
      + kmInvoicePrice(vehicle, steps)
    ;
}

/**
 * Get the price of 1km of fuel consumption
 *
 * @param vehicle
 * @param steps
 * @returns {number}
 */
export const kmFuelPrice = (vehicle, steps) => {
  let price = 0;

  for (let i in vehicle.fuels) {
    if (vehicle.fuels.hasOwnProperty(i)) {
      price += pricePerKilometerOfFuel(vehicle.fuels[i])
        * priceGrowth(parseFloat(vehicle.fuels[i].costGrowth), steps)
    }
  }

  return price
}

/**
 * Get the price of 1km of fuel consumption
 *
 * @param vehicle
 * @param steps
 * @returns {number}
 */
export const kmInvoicePrice = (vehicle, steps) => {
  if (steps === 0) { return 0 }
  
  let kmPerSteps = parseInt(vehicle.kmTraveled) / SCALE;
  let price = 0;

  // Trigger all since 0 as we want the average value
  for (let i = 0; i < steps; i += SCALE) {
    for (let j in vehicle.invoices) {
      if (vehicle.invoices.hasOwnProperty(j)) {
        price += triggerInvoiceAtInstant(vehicle.invoices[j], kmPerSteps, i)
      }
    }
  }
  
  return price / (steps * kmPerSteps)
}

/**
 * Get the price of 1km of fuel consumption
 *
 * @param vehicle
 * @param steps
 * @param sale
 * @returns {number}
 */
export const kmCarPrice = (vehicle, steps, sale = 0) => {
  let years = steps / SCALE;

  return (initial(vehicle) - sale) / (years * parseInt(vehicle.kmTraveled))
}

/**
 * Get multiplication coefficient of price
 *
 * @param coefficient
 * @param steps
 * @returns {number}
 */
export const priceGrowth = (coefficient, steps) => {
  let coef = parseFloat(coefficient) / 100 + 1;
  let value = coef;

  for (let i = 0; i < steps / SCALE; i++) {
    value *= coef;
  }

  return value;
};

/**
 * Get price per Km
 *
 * @param fuel
 * @returns {number}
 */
export const pricePerKilometerOfFuel = (fuel) => {
  return parseFloat(fuel.consumption) / 100 // Consumption per kilometer
    * parseFloat(fuel.cost) // Price per unit
    * parseFloat(fuel.proportion) / 100 // Proportion of this fuel type mix
};

/**
 * Get price of invoice if triggered
 *
 * @param invoice
 * @param kmPerStep
 * @param steps
 * @returns {number}
 */
export const triggerInvoiceAtInstant = (invoice, kmPerStep, steps) => {
  // Exclude this invoice temporally
  if (invoice.exclude) {
    return 0
  }

  // Check if triggered
  let period = parseInt(invoice.period);
  let postpone = parseInt(invoice.periodStart);
  let coefficient = invoice.periodType === 'time' ? 1 : kmPerStep;

  // Set from step
  let fromStep = coefficient * steps - postpone;
  let toStep = fromStep + coefficient * SCALE;
  if (invoice.havePeriodStop && toStep > invoice.periodStop) {
    toStep = invoice.periodStop
  }

  if (fromStep < 0 && toStep < period || toStep <= fromStep) {
    return 0
  }

  let fromIteration = Math.floor(fromStep / period);
  let toIteration = Math.floor(toStep / period);

  // Recurrent
  if (fromIteration !== toIteration) {
    // Check if already included
    if (!invoice.isRecurrent) {
      // Haven't been done
      if (fromIteration <= 0) {
        return parseInt(invoice.cost)
      }

      return 0
    }

    // Get cost times number of trigger
    return parseInt(invoice.cost) * (toIteration - fromIteration)
  }

  return 0
};

/**
 * @param vehicle
 * @param end
 * @returns {Array}
 */
export const chartData = (vehicle, end) => {
  let data = [];

  data.push(initial(vehicle));

  for (let i = SCALE; i <= end; i += SCALE) {
    let prev = typeof data[data.length-1] !== "undefined" ? data[data.length-1] : initial(vehicle);  
    
    data.push(parseInt(price(vehicle, i - SCALE)) + prev)
  }

  return data
};

/**
 * @param vehicle
 * @param end
 * @returns {Array}
 */
export const chartDetail = (vehicle, end) => {
  let data = [];
  let total = initial(vehicle);
  let kmPerStep = parseInt(vehicle.kmTraveled) / SCALE;

  data.push({
    year: 0,
    month: 0,
    change: 0,
    growth: 0,
    total: total.toFixed(2),
    km_total: 0,
    km_price: total.toFixed(2),
    _showDetails: false,
    sortable: true
  });

  for (let i = SCALE; i <= end; i += SCALE) {
    let growth = price(vehicle, i - SCALE);
    total += growth;
    
    let change = growth > data[data.length - 1].growth ? 1 : -1;
    if (growth === data[data.length - 1]) {
      change = 0;
    }

    data.push({
      year: i / SCALE,
      month: i,
      change: change,
      growth: growth.toFixed(2),
      total: total.toFixed(2),
      km_total: (i * kmPerStep).toFixed(0),
      km_price: (kmPrice(vehicle, i)).toFixed(2),
      _showDetails: false
    })
  }

  return data
};

/**
 * @param vehicle
 * @param end
 * @returns {number}
 */
export const totalCost = (vehicle, end) => {
  let total = initial(vehicle);

  for (let i = SCALE; i <= end; i += SCALE) {
    total += price(vehicle, i - SCALE)
  }

  return total
};
