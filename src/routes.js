import About from "./components/About";
import Create from './components/Create'
import Home from './components/Home'

export default [
    { path: '/', name: 'home', component: Home },
    { path: '/create', name: 'create', component: Create },
    { path: '/about', name: 'about', component: About },
]
