import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
import vehicle from "./vehicle"

Vue.use(Vuex)
const vuexPersist = new VuexPersist({
    key: 'vcc',
    storage: window.localStorage
})

export const store = new Vuex.Store({
    namespaced: true,
    state: {

    },
    modules: {
        vehicle
    },
    actions: {
        reset () {
            window.localStorage.removeItem('vcc');

            window.location.href = process.env.NODE_ENV === 'production'
              ? '/vehicle-cost-calculator/'
              : '/';
        }
    },
    plugins: [vuexPersist.plugin]
})
