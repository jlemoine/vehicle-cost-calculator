const ro_preset = {
  zoe: [
    {
      id: 0,
      name: 'Renault Zoe',
      priceBuy: 23200,
      priceSell: 0,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Electricity',
          cost: 0.155, // € per unit
          costGrowth: 2, // %
          consumption: 15, // Units per 100km
          proportion: 100, // % total mix, must be 100%
        },
      ],
      invoices: [
        {
          name: 'State discount',
          cost: -6000,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Old car destruction discount',
          cost: -2500,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Insurance',
          cost: 500,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Battery rent - 15k km',
          cost: 99,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 250,
          periodType: 'distance',
          period: 35000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 300,
          periodType: 'distance',
          period: 50000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    },
    {
      id: 1,
      name: 'Renault Zoe LOA',
      priceBuy: 6000,
      priceSell: 0,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Electricity',
          cost: 0.155, // € per unit
          costGrowth: 2, // %
          consumption: 15, // Units per 100km
          proportion: 100, // % total mix, must be 100%
        },
      ],
      invoices: [
        {
          name: 'State discount',
          cost: -6000,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Old car destruction discount',
          cost: -2500,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Insurance',
          cost: 500,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'LOA',
          cost: 253.16,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: true,
          periodStop: 37,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'LOA Buy option',
          cost: 9051.48,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 37, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Battery rent - 15k km',
          cost: 99,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 250,
          periodType: 'distance',
          period: 35000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 300,
          periodType: 'distance',
          period: 50000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    },
    {
      id: 2,
      name: 'Renault Zoe + battery',
      priceBuy: 32100,
      priceSell: 0,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Electricity',
          cost: 0.155, // € per unit
          costGrowth: 2, // %
          consumption: 15, // Units per 100km
          proportion: 100, // % total mix, must be 100%
        },
      ],
      invoices: [
        {
          name: 'State discount',
          cost: -6000,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Old car destruction discount',
          cost: -2500,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Insurance',
          cost: 500,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 250,
          periodType: 'distance',
          period: 35000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 300,
          periodType: 'distance',
          period: 50000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Battery',
          cost: 8990,
          periodType: 'time',
          period: 96,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    }
  ],
  ice: [
    {
      id: 0,
      name: 'Renault Twingo',
      priceBuy: 11400,
      priceSell: 2500,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Gasoline',
          cost: 1.55,
          costGrowth: 2,
          consumption: 5.0,
          proportion: 100,
        },
      ],
      invoices: [
        {
          name: 'Insurance',
          cost: 500,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 200,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 300,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Timing belt',
          cost: 500,
          periodType: 'distance',
          period: 80000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Alternator',
          cost: 500,
          periodType: 'distance',
          period: 120000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    },
    {
      id: 1,
      name: 'Peugeot 308',
      priceBuy: 21050,
      priceSell: 7000,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Diesel',
          cost: 1.50,
          costGrowth: 2,
          consumption: 6.5,
          proportion: 100,
        },
      ],
      invoices: [
        {
          name: 'Insurance',
          cost: 600,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 250,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 250,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Oil change',
          cost: 120,
          periodType: 'distance',
          period: 30000,
          periodStart: 15000, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Timing belt',
          cost: 550,
          periodType: 'distance',
          period: 80000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Alternator',
          cost: 500,
          periodType: 'distance',
          period: 120000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    },
    {
      id: 2,
      name: 'Used Peugeot 307',
      priceBuy: 7000,
      priceSell: 0,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Diesel',
          cost: 1.50,
          costGrowth: 2,
          consumption: 6.5,
          proportion: 100,
        },
      ],
      invoices: [
        {
          name: 'Insurance',
          cost: 450,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 250,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 250,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Oil change',
          cost: 120,
          periodType: 'distance',
          period: 30000,
          periodStart: 15000, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Timing belt',
          cost: 550,
          periodType: 'distance',
          period: 80000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Alternator',
          cost: 500,
          periodType: 'distance',
          period: 120000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    },
    {
      id: 1,
      name: 'Renault Clio',
      priceBuy: 17800,
      priceSell: 4500,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Gasoline',
          cost: 1.55,
          costGrowth: 2,
          consumption: 5.6,
          proportion: 100,
        },
      ],
      invoices: [
        {
          name: 'Insurance',
          cost: 500,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 200,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 300,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Timing belt',
          cost: 500,
          periodType: 'distance',
          period: 80000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Alternator',
          cost: 500,
          periodType: 'distance',
          period: 120000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    }
  ],
  ve_ice: [
    {
      id: 0,
      name: 'Renault Zoe',
      priceBuy: 23200,
      priceSell: 0,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Electricity',
          cost: 0.155, // € per unit
          costGrowth: 2, // %
          consumption: 15, // Units per 100km
          proportion: 100, // % total mix, must be 100%
        },
      ],
      invoices: [
        {
          name: 'State discount',
          cost: -6000,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Old car destruction discount',
          cost: -2500,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: false,
          exclude: false,
        },
        {
          name: 'Insurance',
          cost: 500,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Battery rent - 15k km',
          cost: 99,
          periodType: 'time', // put period in months or KM
          period: 1,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 250,
          periodType: 'distance',
          period: 35000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 300,
          periodType: 'distance',
          period: 50000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    },
    {
      id: 1,
      name: 'Renault Clio',
      priceBuy: 17800,
      priceSell: 4500,
      kmTraveled: 15000, // Per years
      fuels: [
        {
          name: 'Gasoline',
          cost: 1.55,
          costGrowth: 2,
          consumption: 5.6,
          proportion: 100,
        },
      ],
      invoices: [
        {
          name: 'Insurance',
          cost: 500,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 200,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 300,
          periodType: 'distance',
          period: 30000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical validation',
          cost: 69,
          periodType: 'time',
          period: 24,
          periodStart: 48, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Timing belt',
          cost: 500,
          periodType: 'distance',
          period: 80000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Alternator',
          cost: 500,
          periodType: 'distance',
          period: 120000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    }
  ],
  scooter: [
    {
      id: 0,
      name: 'Super Socco TC',
      priceBuy: 3299,
      priceSell: 1000,
      kmTraveled: 7000, // Per years
      fuels: [
        {
          name: 'Electricity',
          cost: 0.155,
          costGrowth: 2,
          consumption: 3,
          proportion: 100,
        },
      ],
      invoices: [
        {
          name: 'Insurance',
          cost: 170,
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 150,
          periodType: 'distance',
          period: 10000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 70,
          periodType: 'time',
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Breaks',
          cost: 50,
          periodType: 'distance',
          period: 15000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Battery',
          cost: 990,
          periodType: 'distance',
          period: 50000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    },
    {
      id: 1,
      name: 'MASH FIFTY 50cc',
      priceBuy: 1850,
      priceSell: 1000,
      kmTraveled: 7000, // Per years
      fuels: [
        {
          name: 'Gasoline',
          cost: 1.55,
          costGrowth: 2,
          consumption: 2.5,
          proportion: 100,
        },
      ],
      invoices: [
        {
          name: 'Insurance',
          cost: 170, // Should be more
          periodType: 'time', // put period in months or KM
          period: 12,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Wheels',
          cost: 150,
          periodType: 'distance',
          period: 10000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Technical check',
          cost: 70,
          periodType: 'distance',
          period: 3000,
          periodStart: -2000, // from start, first at 1000km
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Breaks',
          cost: 50,
          periodType: 'distance',
          period: 15000,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Candles',
          cost: 50,
          periodType: 'distance',
          period: 6000,
          periodStart: 1000, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Filters',
          cost: 50,
          periodType: 'distance',
          period: 3000,
          periodStart: 1000, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Oil',
          cost: 30,
          periodType: 'distance',
          period: 3000,
          periodStart: 1000, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
        {
          name: 'Battery',
          cost: 50,
          periodType: 'time',
          period: 48,
          periodStart: 0, // from start
          havePeriodStop: false,
          periodStop: 0,
          isRecurrent: true,
          exclude: false,
        },
      ]
    },
  ]
};

export default {
  namespaced: true,
  state: {
    focus: null, // Show data detail on vehicule index or graph if null
    update: null, // Show form off vehicule index or new vehicle
    vehicles: [
    ]
  },
  mutations: {
    preset(state, preset) {
      // Set preset to defined or empty
      state.vehicles = ro_preset[preset] || [];
    },
    purge(state) {
      state.vehicles = [];
    },
    focus(state, index) {
      state.focus = index;
    },
    update(state, index) {
      state.update = index;
    },
    addVehicle(state, vehicle) {
      vehicle.id = state.vehicles.length;
      state.vehicles.push(vehicle)
    },
    updateVehicle(state, vehicle) {
      state.vehicles.splice(vehicle.id, 1, vehicle)
    },
    remove(state, index) {
      state.vehicles.splice(index, 1)
    },
  },
  actions: {
    preset({dispatch, commit}, preset) {
      dispatch('purge');
      commit('preset', preset);
    },
    purge({dispatch, commit}) {
      dispatch('focus', null);
      commit('purge')
    },
    focus({commit}, index) {
      commit('focus', index)
    },
    update({commit}, index) {
      commit('update', index)
    },
    addVehicle({commit}, vehicle) {
      commit('addVehicle', vehicle)
    },
    updateVehicle({dispatch, commit}, vehicle) {
      commit('updateVehicle', vehicle)
      dispatch('update', null)
    },
    remove({dispatch, commit}, index) {
      commit('remove', index);
      dispatch('update', null);
      dispatch('focus', null)
    },
  },
}
