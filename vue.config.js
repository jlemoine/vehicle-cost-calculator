module.exports = {
  devServer: {
    disableHostCheck: true,
    sockHost: 'www.vcc.test'
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/vehicle-cost-calculator/'
    : '/'
}
